local _log = log
local function log(msg, force)
    if force or BCCFSMenu._data.debug then
        _log("[BCCFS Reborn] " .. msg)
    end
end

local BCCFS_WP_NAME = "BCCFS_Waypoint_CookThis"
local BCCFS_DIALOG_IDS = {

    -- rats / cook off
    ["pln_rt1_20"] = "mu",
    ["pln_rt1_22"] = "cs",
    ["pln_rt1_24"] = "hcl",
    ["pln_rt1_12"] = "cooking",

    -- lab rats
    ["pln_rat_stage1_20"] = "mu",
    ["pln_rat_stage1_22"] = "cs",
    ["pln_rat_stage1_24"] = "hcl",
    ["Play_pln_nai_08"] = "cooking",

    -- border crystals
    ["Play_loc_mex_cook_03"] = "mu",
    ["Play_loc_mex_cook_04"] = "cs",
    ["Play_loc_mex_cook_05"] = "hcl",
    ["Play_loc_mex_cook_22"] = "cooking",

}

local init_original = DialogManager.init
function DialogManager:init(...)
    if game_state_machine then
        local mission = (game_state_machine:gamemode().id == GamemodeCrimeSpree.id) and managers.crime_spree:current_mission() or managers.job:current_job_id()
        log('mission: ' .. mission)
        self.bccfs_heist = mission
        if managers.user:get_setting("mute_heist_vo") == true then
            log('heist voice force enabled.', true)
            managers.user:set_setting("mute_heist_vo", false)
        end
    end
    return init_original(self, ...)
end

local queue_dialog_original = DialogManager.queue_dialog
function DialogManager:queue_dialog(id, params, ...)
    local dlg = BCCFS_DIALOG_IDS[id]
    if dlg then
        self:getBCCFSWaypoints()
        if dlg == "mu" or dlg == "cs" or dlg == "hcl" then
            if not self.bccfs_waypoint then
                local position = self.bccfs_positions[dlg]
                local settings = BCCFSMenu._data
                if settings.chat_message then
                    managers.chat:send_message(1, managers.network.account:username() or "Offline", dlg)
                end
                if position and settings.show_waypoint then
                    self.bccfs_waypoint = true
                    managers.hud:add_waypoint(
                        BCCFS_WP_NAME, {
                        icon = 'equipment_vial',
                        distance = true,
                        position = position:ToVector3(),
                        no_sync = true,
                        present_timer = 0,
                        state = "present",
                        radius = 50,
                        color = Color(1, settings.RH, settings.GS, settings.BV),
                        blend_mode = "add"
                    })
                end
            end
        else
            if self.bccfs_waypoint then
                self.bccfs_waypoint = false
                managers.hud:remove_waypoint(BCCFS_WP_NAME)
            end
        end
        log(" - dialog id: " .. id)
    else
        log("unknown dialog id: " .. id)
    end
    return queue_dialog_original(self, id, params, ...)
end

function DialogManager:getBCCFSWaypoints()
    if managers.interaction and not self.bccfs_positions then
        self.bccfs_positions = {}
        if self.bccfs_heist == "nail" or self.bccfs_heist == "cook_off" then
            self.bccfs_positions["mu"] = "157.121475,-635.353210,1863.831177"
            self.bccfs_positions["cs"] = "-3790.888916,450.120789,2049.024658"
            self.bccfs_positions["hcl"] = "-5518.002441,-555.464966,1236.319580"
            log('using hardcoded lab rats waypoint positions.')
        else
            log('trying to find waypoint positions...')
            for _, unit in pairs(managers.interaction._interactive_units) do
                local object, pos = string.sub(unit:name():t(), 1, 10), unit:interaction():interact_position()
                if object == "@ID4111102" then
                    self.bccfs_positions["mu"] = tostring(pos.x) .. "," .. tostring(pos.y) .. "," .. tostring(pos.z + 30)
                    log('mu found: ' .. self.bccfs_positions["mu"])
                elseif object == "@ID498cdfa" then
                    self.bccfs_positions["cs"] = tostring(pos.x) .. "," .. tostring(pos.y) .. "," .. tostring(pos.z + 30)
                    log('cs found: ' .. self.bccfs_positions["cs"])
                elseif object == "@IDdafe5d9" then
                    self.bccfs_positions["hcl"] = tostring(pos.x) .. "," .. tostring(pos.y) .. "," .. tostring(pos.z + 30)
                    log('hcl found: ' .. self.bccfs_positions["hcl"])
                end
            end
            if not (self.bccfs_positions["mu"] and self.bccfs_positions["cs"] and self.bccfs_positions["hcl"]) then
                log('cannot find waypoint positions.')
            end
        end
    end
end
